cmake_minimum_required(VERSION 3.10)
project (DefaultInstall)

include(ExternalProject)

function(SubProject_Add subproject depends)
    ExternalProject_Add(${subproject}
        DEPENDS ${depends}
        PREFIX       ${CMAKE_CURRENT_BINARY_DIR}/${subproject}
        SOURCE_DIR   ${CMAKE_CURRENT_SOURCE_DIR}/${subproject}
        INSTALL_DIR  ${CMAKE_CURRENT_BINARY_DIR}/install_dir
        TMP_DIR      ${CMAKE_CURRENT_BINARY_DIR}/${subproject}/build/tmp
        STAMP_DIR    ${CMAKE_CURRENT_BINARY_DIR}/${subproject}/build/stamp
        BINARY_DIR   ${CMAKE_CURRENT_BINARY_DIR}/${subproject}/build
        DOWNLOAD_DIR ${CMAKE_CURRENT_BINARY_DIR}/download
        CMAKE_ARGS
#            -D CMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
            -D CMAKE_PREFIX_PATH=${CMAKE_CURRENT_BINARY_DIR}/install_dir
            -D CMAKE_INSTALL_PREFIX=${CMAKE_CURRENT_BINARY_DIR}/install_dir
    )
endfunction()

SubProject_Add(A "")
SubProject_Add(B A)
SubProject_Add(C A)
SubProject_Add(D A)
SubProject_Add(E A)
SubProject_Add(X "B;C;D;E")

ExternalProject_Add(DefaultInstallTestExecutable
    DEPENDS X
    PREFIX       ${CMAKE_CURRENT_BINARY_DIR}/DefaultInstallTestExecutable
    SOURCE_DIR   ${CMAKE_CURRENT_SOURCE_DIR}/DefaultInstallTestExecutable
    INSTALL_DIR  ${CMAKE_CURRENT_BINARY_DIR}
    TMP_DIR      ${CMAKE_CURRENT_BINARY_DIR}/DefaultInstallTestExecutable/build/tmp
    STAMP_DIR    ${CMAKE_CURRENT_BINARY_DIR}/DefaultInstallTestExecutable/build/stamp
    BINARY_DIR   ${CMAKE_CURRENT_BINARY_DIR}/DefaultInstallTestExecutable/build
    DOWNLOAD_DIR ${CMAKE_CURRENT_BINARY_DIR}/download
    CMAKE_ARGS
        -D CMAKE_PREFIX_PATH=${CMAKE_CURRENT_BINARY_DIR}/install_dir
        -D CMAKE_INSTALL_PREFIX=${CMAKE_CURRENT_BINARY_DIR}
)
