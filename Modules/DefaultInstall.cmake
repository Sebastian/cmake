# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

#[=======================================================================[.rst:
DefaultInstall
==============

Functions to simplify installing targets

A module that encapsulates system differences in installing
libraries (static and shared) and their headers, and executables.

The following functions are provided by this module:

::

   default_install

The following functions are modified by this module:

::

   find_package
   install


Simplified installation of headers
----------------------------------

.. command:: install(TARGETS ...)

  The :command:`install` command is extended with a new parameter keyword.
  If you don't use it, it behaves as before.

    ``INTERFACE_SOURCES DESTINATION <destination>``
      Directory into which files shall be installed which are in the targets
      ``INTERFACE_SOURCES`` property. This property is easiest populated from
      the ``PUBLIC`` section of the :command:`target_sources` command.

  When an ``INTERFACE_SOURCES DESTINATION`` is given, the install command will
  install all interface sources into that destination.

  In addition, it will change the ``INCLUDE_DIRECTORIES`` property of the target
  so that they are found there.
  In the :command:`target_include_directories` command, you don't need to use
  :manual:`generator expressions <cmake-generator-expressions(7)>`
  for that anymore.
  This works under one assumption: The interface sources must be referenced
  with the shortest possible path for the given ``INCLUDE_DIRECTORIES``.
  For example::

    target_sources(ExampleTarget
        PUBLIC
            ${CMAKE_CURRENT_SOURCE_DIR}/a/b/c/d.h
            ${CMAKE_CURRENT_SOURCE_DIR}/a/e.h
    )

    target_include_directories(ExampleTarget PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}/a/b
        ${CMAKE_CURRENT_SOURCE_DIR}/a
    )

    install(TARGETS ExampleTarget
        INTERFACE_SOURCES DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/i
    )

  Here tere are two include directories, maybe because you want to include the
  headers like this::

    #include "c/d.h"
    #include "e.h"

  Up to here, all fine. But what will not work is this::

    #include "b/c/d.h"

  Before installing, this worked. But after installing, ``d.h`` is in
  ``${CMAKE_INSTALL_INCLUDEDIR}/i/c/d.h``, not in
  ``${CMAKE_INSTALL_INCLUDEDIR}/i/b/c/d.h``.

  So if you have this uncommon case in your code base, sorry. You can't use the
  ``INTERFACE_SOURCES DESTINATION``, and you need to install the old way, with
  ``install(FILES ...)`` and generator expressions for your
  ``target_include_directories``.

Simplified installation of package version files
------------------------------------------------

.. command:: install(VERSION ...)

  The :command:`install` command can now create and install package version files.
  It's a combination of :command:`write_basic_package_version_file`
  and :command:`install(FILES ...)`.

    ``VERSION <version>``
      Same as for :command:`write_basic_package_version_file`

    ``COMPATIBILITY <compatibility>``
      Same as for :command:`write_basic_package_version_file`

    ``DESTINATION <destination>``
      Same as for :command:`install(FILES ...)`

    ``FILE <file>``
      Name of the package version file, without directory.
      You probably want to use something that's recognized by
      :command:`find_package`, that is
      ``"${PROJECT_NAME}ConfigVersion.cmake"``
      or ``"${PROJECT_NAME}-config-version.cmake"``

      We could use the first one as default, if the parameter is not given.
      But :command:`install(EXPORT ...)` also doesn't do that, and
      when you want deafult values you can use :command:`default_install`

.. command:: install(CONFIG ...)

  Create and install package configuration files.

  **Not implemened yet.**
  Is there a use case, or is the EXPORTed one almost always good enough?


Simplified installation of package dependencies
-----------------------------------------------

This solves the problem described in
`The Ultimate Guide to Modern CMake`_
at the end of section *Importing external libraries*.

.. command:: find_package(... EXPORT <export-name>)

  Records the package dependency (name, version)
  for later use, similar to
  :command:`install(TARGETS ... EXPORT ...)`

  If you omit the ``EXPORT`` parameter (that is, you use :command:`find_package`
  as usual), the dependency will be recorded anyway in a special export-set
  which is used implicitly by :command:`default_install` if you omit the
  ``EXPORT`` parameter there, too.

.. command:: install(EXPORT ...)

  Same as before, but it also puts :command:`find_package` calls for all
  recorded packages into the generated package configuration file.


Simplified installation of packages
-----------------------------------

``CMAKE_INSTALL_CONFIGDIR``

  A default directory for installation of cmake package configuration files.
  This is an attempt to solve a part of `Issue 17238`_. The value might change
  when the discussion there comes to a result, but for you as a library developer,
  it shouldn't matter.

.. command:: default_install

  Convenience command for instaling a package. If you're happy with the
  provided defaults, installation becomes a one-liner::

    default_install(TARGETS <targets>)

  Otherwise, you can customize a few things:

    ``EXPORT <export-name>``
      Same as for :command:`install(EXPORT ...)`

    ``COMPATIBILITY <compatibility>``
      Same as for :command:`write_basic_package_version_file`


.. _`The Ultimate Guide to Modern CMake`:
   https://rix0r.nl/blog/2015/08/13/cmake-guide/

.. _`Issue 17238`:
   https://gitlab.kitware.com/cmake/cmake/issues/17238

#]=======================================================================]

include(CMakePackageConfigHelpers)
include(GNUInstallDirs)
set(CMAKE_INSTALL_CONFIGDIR share/${CMAKE_PROJECT_NAME}/cmake)
set(CMAKE_INSTALL_EXPORT_TMPDIR "${CMAKE_BINARY_DIR}/export")

function(target_install_interface_sources target destination)
    set(new_INTERFACE_SOURCES "")
    get_target_property(old_INTERFACE_SOURCES ${target} INTERFACE_SOURCES)
    get_target_property(old_INCLUDE_DIRECTORIES ${target} INTERFACE_INCLUDE_DIRECTORIES)
    foreach(source ${old_INTERFACE_SOURCES})
        set(longest 0)
        foreach(dir ${old_INCLUDE_DIRECTORIES})
            string(LENGTH ${dir} dir_length)
            string(SUBSTRING ${source} 0 ${dir_length} source_start)
            if(dir STREQUAL source_start)
                string(SUBSTRING ${source} ${dir_length} -1 source_rest)
                if(longest LESS dir_length)
                    set(longest ${dir_length})
                    set(new_source $<BUILD_INTERFACE:${dir}>$<INSTALL_INTERFACE:${destination}>${source_rest})
                    get_filename_component(best_destination ${destination}${source_rest} DIRECTORY)
                endif()
            endif()
        endforeach()
        install(FILES ${source} DESTINATION ${best_destination})
        list(APPEND new_INTERFACE_SOURCES ${new_source})
    endforeach()
    set_target_properties(${target} PROPERTIES INTERFACE_SOURCES "${new_INTERFACE_SOURCES}")

    set(new_INCLUDE_DIRECTORIES $<BUILD_INTERFACE:${old_INCLUDE_DIRECTORIES}>$<INSTALL_INTERFACE:${destination}>)
    set_target_properties(${target} PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${new_INCLUDE_DIRECTORIES}")
endfunction()

function(install_export_dependency_packages file include_file destination packages)
    get_filename_component(file_name "${file}" NAME)
    file(WRITE "${CMAKE_INSTALL_EXPORT_TMPDIR}/${file_name}"
        "include(\${CMAKE_CURRENT_LIST_DIR}/${include_file})")
    foreach(package ${packages})
        file(APPEND "${CMAKE_INSTALL_EXPORT_TMPDIR}/${file_name}"
            "
find_package(${package})")
    endforeach()
    install(FILES "${CMAKE_INSTALL_EXPORT_TMPDIR}/${file_name}" DESTINATION ${destination})
endfunction()

function(install)
    # First run of cmake_parse_arguments to extract a few things we want to keep in UNPARSED_ARGUMENTS, later.
    # Have all other keywords in the "options" list so that they work as delimiters.
    set(options INTERFACE_SOURCES EXPORT ARCHIVE LIBRARY RUNTIME OBJECTS FRAMEWORK BUNDLE PRIVATE_HEADER PUBLIC_HEADER RESOURCE INCLUDES PERMISSIONS CONFIGURATIONS COMPONENT OPTIONAL NAMELINK_ONLY NAMELINK_SKIP)
    set(oneValueArgs DESTINATION)
    set(multiValueArgs TARGETS)
    cmake_parse_arguments(FIRST_PARSE "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    # Second run of cmake_parse_arguments which extracts INTERFACE_SOURCES DESTINATION, and has an
    # UNPARSED_ARGUMENTS that has everything except INTERFACE_SOURCES DESTINATION.
    string(REPLACE "INTERFACE_SOURCES;DESTINATION" "INTERFACE_SOURCES_DESTINATION" ARGN "${ARGN}")
    set(options "")
    set(oneValueArgs INTERFACE_SOURCES_DESTINATION)
    set(multiValueArgs "")
    cmake_parse_arguments(MY_INSTALL "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
    if(DEFINED MY_INSTALL_INTERFACE_SOURCES_DESTINATION)
        foreach(target ${FIRST_PARSE_TARGETS})
            target_install_interface_sources(${target} ${MY_INSTALL_INTERFACE_SOURCES_DESTINATION})
        endforeach()
        _install(${MY_INSTALL_UNPARSED_ARGUMENTS})
    elseif(ARGV0 STREQUAL "EXPORT" AND DEFINED ${ARGV1}_DEPENDENCY_PACKAGES)
        set(options "")
        set(oneValueArgs FILE)
        set(multiValueArgs "")
        cmake_parse_arguments(INSTALL_EXPORT "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

        get_filename_component(file_ext "${INSTALL_EXPORT_FILE}" EXT)
        string(REPLACE "${file_ext}" "Include${file_ext}" file_renamed "${INSTALL_EXPORT_FILE}")
        set(new_ARGN "${INSTALL_EXPORT_UNPARSED_ARGUMENTS}")
        list(APPEND new_ARGN "FILE;${file_renamed}")

        install_export_dependency_packages("${INSTALL_EXPORT_FILE}" "${file_renamed}" "${FIRST_PARSE_DESTINATION}" "${${ARGV1}_DEPENDENCY_PACKAGES}")
        _install(${new_ARGN})
        # You could also modify the file without renaming and including. Less nice?
        #_install(CODE "message(\"EXPORT ${CMAKE_INSTALL_PREFIX} ${ARGN}\")")
    elseif(ARGV0 STREQUAL "VERSION")
        set(options "")
        set(oneValueArgs VERSION COMPATIBILITY DESTINATION FILE)
        set(multiValueArgs "")
        cmake_parse_arguments(INSTALL_VERSION "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

        write_basic_package_version_file(
            "${CMAKE_INSTALL_EXPORT_TMPDIR}/${INSTALL_VERSION_FILE}"
            VERSION ${INSTALL_VERSION_VERSION}
            COMPATIBILITY ${INSTALL_VERSION_COMPATIBILITY}
        )
        install(FILES
            "${CMAKE_INSTALL_EXPORT_TMPDIR}/${INSTALL_VERSION_FILE}"
            DESTINATION ${INSTALL_VERSION_DESTINATION})
    else()
        _install(${ARGN})
    endif()
endfunction()

# Must be a macro, otherwise variables like ..._FOUND won't get into the calling scope.
macro(find_package)
    cmake_parse_arguments(MY_FIND_PACKAGE "" "EXPORT" "" ${ARGN} )
    if(NOT DEFINED MY_FIND_PACKAGE_EXPORT)
         set(MY_FIND_PACKAGE_EXPORT "${PROJECT_NAME}_IMPLICIT_EXPORT_SET_NAME")
    endif()
    set(ARGS ${MY_FIND_PACKAGE_UNPARSED_ARGUMENTS})
    list(REMOVE_ITEM ARGS QUIET)
    list(REMOVE_ITEM ARGS REQUIRED)
    string(REPLACE ";" " " ARGS "${ARGS}")
    list(APPEND ${MY_FIND_PACKAGE_EXPORT}_DEPENDENCY_PACKAGES "${ARGS}")
    _find_package(${MY_FIND_PACKAGE_UNPARSED_ARGUMENTS})
    # TODO: Setting a lot of variables here. Should be scoped (maybe do the work needing variables in a helper function?)
    # But at least one variable, with the arguments for _find_package, must escape that function...
    # unset is not a perfect solution - if  the variable was set before, it's gone now...
    unset(ARGS)
    unset(MY_FIND_PACKAGE_EXPORT)
    unset(MY_FIND_PACKAGE_UNPARSED_ARGUMENTS)
endmacro()

function(default_install)
    set(options "")
    set(oneValueArgs EXPORT COMPATIBILITY)
    set(multiValueArgs TARGETS)
    cmake_parse_arguments(DEFAULT_INSTALL "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    if(NOT DEFINED DEFAULT_INSTALL_COMPATIBILITY)
        set(DEFAULT_INSTALL_COMPATIBILITY SameMajorVersion)
    endif()

    if(NOT DEFINED DEFAULT_INSTALL_EXPORT)
        set(DEFAULT_INSTALL_EXPORT "${PROJECT_NAME}_IMPLICIT_EXPORT_SET_NAME")
    endif()

    set(CONFIG_FILE "${PROJECT_NAME}Config.cmake")
    set(VERSION_FILE "${PROJECT_NAME}ConfigVersion.cmake")

    install(VERSION ${PROJECT_VERSION}
        COMPATIBILITY ${DEFAULT_INSTALL_COMPATIBILITY}
        DESTINATION ${CMAKE_INSTALL_CONFIGDIR} FILE ${VERSION_FILE})
    if(DEFINED DEFAULT_INSTALL_TARGETS)
        foreach(target ${DEFAULT_INSTALL_TARGETS})
            set_target_properties(${target} PROPERTIES DEBUG_POSTFIX "d")
        endforeach()
        install(TARGETS ${DEFAULT_INSTALL_TARGETS} EXPORT ${DEFAULT_INSTALL_EXPORT}
            INTERFACE_SOURCES DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
            ARCHIVE  DESTINATION ${CMAKE_INSTALL_LIBDIR}
            LIBRARY  DESTINATION ${CMAKE_INSTALL_LIBDIR}
            RUNTIME  DESTINATION ${CMAKE_INSTALL_BINDIR})
    endif()
    install(EXPORT ${DEFAULT_INSTALL_EXPORT}
        DESTINATION ${CMAKE_INSTALL_CONFIGDIR} FILE ${CONFIG_FILE})
endfunction()
